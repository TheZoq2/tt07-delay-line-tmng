# Delay line based time multiplexed NAND gate

This is a single NAND gate that is fed its inputs from, and writes its results
to a giant shift register. With this, we can achieve the ultimate time/space
tradeoff, a single nand gate able to emulate quite complex logic. 
