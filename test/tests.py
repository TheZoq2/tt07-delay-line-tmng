#top = main::tmng

from cocotb.clock import Clock
from spade import FallingEdge, SpadeExt
from cocotb import cocotb

LENGTH = 1100;

@cocotb.test()
async def full_of_ones_works(dut):
    s = SpadeExt(dut) # Wrap the dut in the Spade wrapper

    clk = dut.clk_i

    await cocotb.start(Clock(
        clk,
        period=10,
        units='ns'
    ).start())

    for i in range(0, LENGTH):
        s.i.ctrl = "Input$(write_nand: false, write_input: true, input_: true, read_opa: false, read_opb: false, write_output: false)";
        await FallingEdge(clk)

    # We have now wrapped around. Write into the output sreg 8 times
    for _ in range(0, 8):
        s.i.ctrl = "Input$(write_nand: false, write_input: false, input_: true, read_opa: false, read_opb: false, write_output: true)";
        await FallingEdge(clk)
    await FallingEdge(clk)

    s.o.assert_eq("0b1111_1111")



@cocotb.test()
async def basic_nand_truth_table(dut):
    s = SpadeExt(dut) # Wrap the dut in the Spade wrapper

    clk = dut.clk_i

    await cocotb.start(Clock(
        clk,
        period=10,
        units='ns'
    ).start())

    inputs = [0, 0, 0,  0, 1, 0,  1, 0, 0,  1, 1, 0]
            #       ^         ^         ^         ^ Output placeholders
    for i in range(0, LENGTH):
        if i < len(inputs):
            s.i.ctrl = f"Input$(write_nand: false, write_input: true, input_: {'true' if inputs[i] else 'false'}, read_opa: false, read_opb: false, write_output: false)";
        else:
            s.i.ctrl = f"Input$(write_nand: false, write_input: true, input_: true, read_opa: false, read_opb: false, write_output: false)";
        await FallingEdge(clk)

    # We have now wrapped around. Perform the nanding
    for _ in range(0, 4):
        s.i.ctrl = "Input$(write_nand: false, write_input: false, input_: true, read_opa: true, read_opb: false, write_output: false)";
        await FallingEdge(clk)
        s.i.ctrl = "Input$(write_nand: false, write_input: false, input_: true, read_opa: false, read_opb: true, write_output: false)";
        await FallingEdge(clk)
        # NOTE: Writing output here to put non-X's there
        s.i.ctrl = "Input$(write_nand: true, write_input: false, input_: true, read_opa: false, read_opb: false, write_output: true)";
        await FallingEdge(clk)
    
    s.i.ctrl = "Input$(write_nand: false, write_input: false, input_: true, read_opa: false, read_opb: false, write_output: false)";

    [await FallingEdge(clk) for i in range(0, LENGTH - 4)]

    # Another wraparound. Write into the output sreg 8 times
    for _ in range(0, 4):
        s.i.ctrl = "Input$(write_nand: false, write_input: false, input_: true, read_opa: false, read_opb: false, write_output: false)";
        s.i.ctrl = "Input$(write_nand: false, write_input: false, input_: true, read_opa: false, read_opb: false, write_output: false)";
        s.i.ctrl = "Input$(write_nand: false, write_input: false, input_: true, read_opa: false, read_opb: false, write_output: true)";
        await FallingEdge(clk)
        s.i.ctrl = "Input$(write_nand: false, write_input: false, input_: true, read_opa: false, read_opb: false, write_output: false)";
    await FallingEdge(clk)

    s.o.assert_eq("0b0000_1110")
